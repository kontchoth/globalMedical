from django.http import Http404
from django.shortcuts import render, redirect, render_to_response
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


def dashboard(request):
	if not request.user.is_authenticated:
		return redirect('/auth/login')
	params = {
		'title': 'Dashboard',
		'is_login': True,
		'notifications_count': 9,
		'notifications': [
			{'name': 'Nurse Added', 'count': 3},
			{'name': 'Nurse Suspended', 'count': 3},
			{'name': 'Nurse Hired', 'count': 3}
		]
	}
	return render_to_response('globalMedical/dashboard.html', params)
