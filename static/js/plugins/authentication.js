(function() {
	var auth = {
		fieldsId: ['#email', '#password'],
		canLogin: true,
		loginBtn: '#loginButton',
	};

	this.auth = auth;

	auth.Authenticate = function() {
		var data = JSON.stringify({
			email: $('input#email').val(),
			password: $('input#password').val()
		});

		var options = {
			url: '/auth/login',
			data: data,
			type: 'POST',
			dataType: 'json',
			contentType: 'application/json',
			success: loginFnSuccess,
			error: loginFnError,
		};

		function loginFnSuccess(result,status,xhr){
			document.location = '/dashboard';
		}

		function loginFnError(xhr,status,error) {
			console.log(xhr, status, error);
		}

		$.ajax(options);
	}

	auth.ValidateForm = function() {
		$(auth.loginBtn).removeClass('disabled');
		auth.fieldsId.forEach(function(id){
			if ($('input' + id).hasClass('invalid') || $('input'+id).val().trim() === ''){
				$(auth.loginBtn).addClass('disabled');
			}
		});
	}

	// bind login btn
	$('a#loginButton').click(function() {
		auth.Authenticate();
	});

	// bind on change
	auth.fieldsId.forEach(function(id){
		$('input' + id).on('input', function(ele){
			auth.ValidateForm();
		});
	});

	// validate the form
	setTimeout(auth.ValidateForm, 200);	
	

	


}());