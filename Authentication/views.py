from django.shortcuts import render, render_to_response, redirect
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import authenticate, login, logout
from Authentication.models import User

class LoginView(APIView):
	def get(self, request, format=None):
		params = {
			'title': 'Login'
		}
		if request.user.is_authenticated:
			return redirect('/dashboard')
		return render_to_response('login.html', params)

	def post(self, request, format=None):
		# verify post data
		email = request.data.get('email', None)
		password = request.data.get('password', None)

		if not email or not password:
			return Response({'message': 'Email and/or Password missing.'}, status=status.HTTP_400_BAD_REQUEST)
		user = authenticate(email=email, password=password)
		if user is not None:
			login(request, user)
			return Response(status=status.HTTP_204_NO_CONTENT)
		else:
			return Response({'message': 'Invalid credentials.'}, status=status.HTTP_400_BAD_REQUEST)

class LogoutView(APIView):
	def get(self, request, format=None):
		if request.user.is_authenticated:
			logout(request)
		return redirect('/dashboard', request)
	
	def post(self, request, format=None):
		# will be used for mobile
		if request.user.is_authenticated:
			logout(request)
		return redirect('/dashboard')