from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _ 

from .managers import UserManager

class User(AbstractBaseUser, PermissionsMixin):
	email = models.EmailField(_('Email Address'), unique=True)
	firstname = models.CharField(_('First Name'), max_length=100, blank=True)
	lastname = models.CharField(_('Last Name'), max_length=100, blank=True)
	middlename = models.CharField(_('Middle Name'), max_length=1, blank=True)
	created_at = models.DateTimeField(_('Date Joined'), auto_now_add=True)
	is_active = models.BooleanField(_('Active'), default=True)
	avatar = models.ImageField(upload_to='avatars/', null=True, blank=True)

	objects = UserManager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []

	class Meta:
		verbose_name = _('user')
		verbose_name_plural = _('Users')

	def get_full_name(self):
		return '{} {}'.format(self.firstname, self.lastname)

	def get_short_name(self):
		return self.firstname

	def email_user(self, subject, message, from_email=None, **kwargs):
		send_mail(subject, message, from_email, [self.email], **kwargs)

	