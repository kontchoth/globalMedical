from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from Authentication import views

urlpatterns = [
	url(r'^login$', views.LoginView.as_view()),
	url(r'^api/login$', views.LoginView.as_view()),
	url(r'^logout$', views.LogoutView.as_view()),

]