from Employee.models import Position, Employee
from Employee.serializers import PositionSerializer, EmployeeSerializer

data = {
	'firstname': 'Hermann',
	'middlename': 'T',
	'lastname': 'Kontcho',
	'gender': 'Male',
	'dln': 'K11111111111',
	'ssn': '111223333',
	'streetname': '9709 Byward Boulevard',
	'city': 'Bowie',
	'state': 'MD',
	'zipcode': '20721',
	'position': 1

}

employee = EmployeeSerializer(data=data)
employee.is_valid()