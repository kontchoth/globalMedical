from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from Employee import views

urlpatterns = [
	url(r'^positions/$', views.PositionList.as_view()),
	url(r'^positions/(?P<pk>[0-9]+)/$', views.PositionDetail.as_view()),

	url(r'^employees/$', views.EmployeeList.as_view()),
	url(r'^employees/(?P<pk>[0-9]+)/$', views.EmployeeDetail.as_view()),
	url(r'^employees/(?P<pk>[0-9]+)/personal-information/$', views.PersonalInformationDetail.as_view()),

	url(r'^skills/$', views.SkillList.as_view()),
	url(r'^skills/(?P<pk>[0-9]+)/$', views.SkillDetail.as_view()),
	url(r'^employees/(?P<pk>[0-9]+)/skills/$', views.EmployeeSkillList.as_view()),

	url(r'^notes/$', views.NoteList.as_view()),
	url(r'^employees/(?P<pk>[0-9]+)/notes/$', views.NoteList.as_view()),

	url(r'^certificates/$', views.CertificateList.as_view()),
	url(r'^certificates/(?P<pk>[0-9]+)/$', views.CertificateDetail.as_view()),
	url(r'^employees/(?P<pk>[0-9]+)/certificates/$', views.CertificateList.as_view()),

]