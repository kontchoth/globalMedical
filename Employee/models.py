from __future__ import unicode_literals
from django.utils.translation import gettext as _
from Authentication.models import User

from django.db import models

# Create your models here.
class Position(models.Model):
	abbr = models.CharField(_('Position'), max_length=100, null=False, blank=False)
	description = models.CharField(_('Description'), max_length=200, null=True, blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		ordering = ('abbr', )

	def __str__(self):
		return self.abbr

class PersonalInformation(models.Model):
	GENDER_CHOICES = (
	    ('Male', 'Male'),
	    ('Female', 'Female'),
	)
	STATUS_CHOICES = (
	    ('Active', 'Active'),
	    ('Inactive', 'Inactive'),
	    ('Suspended', 'Suspended'),
	)
	firstname = models.CharField(_('First Name'), max_length=200, blank=False, null=False)
	middlename = models.CharField(_('Middle Name'), max_length=1, blank=True, null=True)
	lastname = models.CharField(_('Last Name'), max_length=200, blank=False, null=False)
	gender = models.CharField(_('Gender'), max_length=200, choices=GENDER_CHOICES, default='Male')
	dln = models.CharField(_('Driver Licence Number'), max_length=200, blank=True, null=True)

	birthday = models.DateField(_('Birth Date'), blank=True, null=True)
	ssn = models.CharField(_('Social Security'), max_length=9, blank=False, null=False)
	streetname = models.CharField(_('Street Name'), max_length=200, blank=True, null=True)
	city = models.CharField(_('City'), max_length=200, blank=True, null=True)
	state = models.CharField(_('State'), max_length=200, blank=True, null=True)
	zipcode = models.CharField(_('Zip Code'), max_length=5, null=True, blank=True)

	hired_date = models.DateField(null=True, blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	position = models.ForeignKey(Position, related_name='position', blank=False, null=False)
	status = models.CharField(_('Status'), max_length=200, choices=STATUS_CHOICES, null=False, blank=False, default='Inactive')

	class Meta:
		ordering = ('firstname', 'lastname', )

	def __str__(self):
		return '{} {}'.format(self.firstname, self.lastname)

class Skill(models.Model):
	name = models.CharField(_('Skill Name'), max_length=200, null=False, blank=False)
	description = models.TextField(_('Skill Description'), null=True, blank=True)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	created_by = models.ForeignKey(User, related_name='created_by', blank=False, null=False)
	updated_by = models.ForeignKey(User, related_name='updated_by', blank=False, null=False)

	class Meta:
		ordering = ('name', )

	def __str__(self):
		return self.name

class Employee(models.Model):
	personal_information = models.ForeignKey(PersonalInformation, blank=True, null=True)

	class Meta:
		ordering = ('personal_information__firstname', 'personal_information__lastname',)

	def __str__(self):
		return str(self.personal_information)

class EmployeeSkill(models.Model):
	employee = models.ForeignKey(Employee, related_name='skills', null=False, blank=False)
	skill = models.ForeignKey(Skill, null=False, blank=False)

	def __str__(self):
		return '{} {}'.format(str(self.employee), str(skill))

	class Meta:
		ordering = ('employee',)
		unique_together = ('employee', 'skill',)

class Note(models.Model):
	employee = models.ForeignKey(Employee, related_name='notes', blank=False, null=False)
	content = models.TextField(null=False, blank=False)
	
	created_by = models.ForeignKey(User, related_name='author', blank=False, null=False)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering = ('employee__id', )

	def __str__(self):
		return 'Create by {} for {}'.format(str(self.created_by), str(self.employee))

class Certificate(models.Model):
	employee = models.ForeignKey(Employee, related_name='certificates', blank=False, null=False)

	name = models.CharField(_('Certification Name'), max_length=200, null=False, blank=False)
	description = models.TextField(_('Description'), default='No description')
	issued_date = models.DateField(_('Issued Date'), blank=False, null=False)
	expiration_date = models.DateField(_('Expiration Date'), blank=False, null=False)
	image = models.FileField(_('Certificate File'), upload_to='certificates/')

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return '{} {}'.format(str(self.employee), self.name)

	def get_employee(self):
		return str(self.employee)


