from rest_framework import serializers
from Employee.models import Position, PersonalInformation, Certificate
from Employee.models import Skill, EmployeeSkill, Employee, Note

class PositionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Position
		fields = '__all__'
		read_only = ('id', 'abbr', 'description', 'created_at', 'updated_at')

	def create(self, validated_data):
		return Position.objects.create(**validated_data)

	def update(self, instance, validated_data):
		instance.abbr = validated_data.get('abbr', instance.abbr)
		instance.description = validated_data.get('description', instance.description)
		instance.save()
		return instance

class PersonalInformationSerializer(serializers.ModelSerializer):
	position = PositionSerializer()
	class Meta:
		model = PersonalInformation
		fields = '__all__'
		read_only = ('id', 'created_at', 'updated_at',)

	def create(self, validated_data):
		# extract the position
		position = Position.objects.get(abbr=validated_data['position'].get('abbr'))
		validated_data['position'] = position
		return PersonalInformation.objects.create(**validated_data)

	def update(self, instance, validated_data):
		# get the position
		position = Position.objects.get(abbr=validated_data['position'].get('abbr'))

		validated_data['position'] = position
		instance.position = validated_data.get('position', instance.position)
		instance.firstname = validated_data.get('firstname', instance.firstname)
		instance.middlename = validated_data.get('middlename', instance.middlename)
		instance.lastname = validated_data.get('lastname', instance.lastname)

		instance.gender = validated_data.get('gender', instance.gender)
		instance.dln = validated_data.get('dln', instance.dln)
		instance.birthday = validated_data.get('birthday', instance.birthday)
		instance.ssn = validated_data.get('ssn', instance.ssn)
		instance.streetname = validated_data.get('streetname', instance.streetname)
		instance.city = validated_data.get('city', instance.city)
		instance.state = validated_data.get('state', instance.state)
		instance.hired_date = validated_data.get('hired_date', instance.hired_date)
		instance.zipcode = validated_data.get('zipcode', instance.zipcode)
		instance.status = validated_data.get('status', instance.status)

		instance.save()
		return instance

class SkillSerializer(serializers.ModelSerializer):
	class Meta:
		model = Skill
		fields = '__all__'
		read_only = ('id', 'name', 'description', 'created_at', 'updated_at', 'created_by')

	def create(self, validated_data):
		return Skill.objects.create(**validated_data)

	def update(self, instance, validated_data):
		instance.name = validated_data.get('name', instance.name)
		instance.description = validated_data.get('description', instance.description)
		instance.update_by = validated_data.get('updated_by', instance.updated_by)
		instance.save()
		return instance

class EmployeeSkillSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = EmployeeSkill
		fields = '__all__'
		read_only = ('id', 'created_at', 'updated_at')

	def create(self, validated_data):
		return EmployeeSkill.objects.create(**validated_data)

	def updated(self, instance, validated_data):
		instance.save()
		return instance

class NoteSerializer(serializers.ModelSerializer):

	class Meta:
		model = Note
		fields = ('employee', 'content', 'created_at', 'created_by' )
		read_only = ('id', 'created_at', 'updated_at', 'content')

	def create(self, validated_data):
		return Note.objects.create(**validated_data)

class CertificateSerializer(serializers.ModelSerializer):
	employee_name = serializers.SerializerMethodField()
	class Meta:
		model = Certificate
		fields = ('id', 'employee', 'employee_name', 'name', 'description', 'issued_date', 
					'expiration_date', 'image', 'created_at', 'updated_at')
		read_only = ('employee_name', 'issued_date', 'created_at', 'updated_at')

	# serializer method field
	def get_employee_name(self, obj):
		return obj.get_employee()

	def create(self, validated_data):
		return Certificate.objects.create(**validated_data)

	def update(self, instance, validated_data):
		instance.name = validated_data.get('name', instance.name)
		instance.issued_date = validated_data.get('issued_date', instance.issued_date)
		instance.expiration_date = validated_data.get('expiration_date', instance.expiration_date)
		instance.image = validated_data.get('image', instance.image)
		instance.description = validated_data.get('description', instance.description)
		instance.save()
		return instance

class EmployeeSerializer(serializers.ModelSerializer):
	personal_information = PersonalInformationSerializer()
	skills = EmployeeSkillSerializer(many=True, read_only=True)
	notes = NoteSerializer(many=True, read_only=True)
	certificates = CertificateSerializer(many=True, read_only=True)

	class Meta:
		model = Employee
		fields = ('personal_information', 'skills', 'notes', 'certificates')
		read_only = ('id', 'created_at', 'updated_at')

	def create(self, validated_data):
		personal_information = validated_data.get('personal_information', None)
		if personal_information:
			personal_information = PersonalInformationSerializer(data=personal_information)
		if personal_information.is_valid():
			personal_information.save()
			personal_information = PersonalInformation.objects.latest('created_at')
			
		return Employee.objects.create(personal_information=personal_information)

	def update(self, instance, validated_data):
		instance.save()
		return instance

