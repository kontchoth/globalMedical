from Employee.models import Position, PersonalInformation, Skill, EmployeeSkill, Employee, Note
from Employee.models import Certificate
from Employee.serializers import PositionSerializer, PersonalInformationSerializer
from Employee.serializers import SkillSerializer, EmployeeSkillSerializer, EmployeeSerializer
from Employee.serializers import NoteSerializer, CertificateSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics, permissions


class PositionList(generics.ListCreateAPIView):
	queryset = Position.objects.all()
	serializer_class = PositionSerializer
	permission_classes = (permissions.IsAuthenticated, )

class PositionDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = PersonalInformation.objects.all()
	serializer_class = PersonalInformationSerializer
	permission_classes = (permissions.IsAuthenticated, )

class PersonalInformationList(generics.ListCreateAPIView):
	queryset = PersonalInformation.objects.all()
	serializer_class = PersonalInformationSerializer
	permission_classes = (permissions.IsAuthenticated, )

class PersonalInformationDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = PersonalInformation.objects.all()
	serializer_class = PersonalInformationSerializer
	permission_classes = (permissions.IsAuthenticated, )

class SkillList(generics.ListCreateAPIView):
	queryset = Skill.objects.all()
	serializer_class = SkillSerializer
	permission_classes = (permissions.IsAuthenticated, )

class SkillDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Skill.objects.all()
	serializer_class = SkillSerializer
	permission_classes = (permissions.IsAuthenticated, )

class EmployeeList(generics.ListCreateAPIView):
	queryset = Employee.objects.all()
	serializer_class = EmployeeSerializer
	permission_classes = (permissions.IsAuthenticated, )

class EmployeeDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Employee.objects.all()
	serializer_class = EmployeeSerializer
	permission_classes = (permissions.IsAuthenticated, )

class EmployeeSkillList(generics.ListCreateAPIView):
	queryset = EmployeeSkill.objects.all()
	serializer_class = EmployeeSkillSerializer
	permission_classes = (permissions.IsAuthenticated, )

class EmployeeSkillDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = EmployeeSkill.objects.all()
	serializer_class = EmployeeSkillSerializer
	permission_classes = (permissions.IsAuthenticated, )

class NoteList(generics.ListCreateAPIView):
	queryset = Note.objects.all()
	serializer_class = NoteSerializer
	permission_classes = (permissions.IsAuthenticated, )

class NoteDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Note.objects.all()
	serializer_class = NoteSerializer
	permission_classes = (permissions.IsAuthenticated, )

class CertificateList(generics.ListCreateAPIView):
	queryset = Certificate.objects.all()
	serializer_class = CertificateSerializer
	permission_classes = (permissions.IsAuthenticated, )

class CertificateDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Certificate.objects.all()
	serializer_class = CertificateSerializer
	permission_classes = (permissions.IsAuthenticated, )
